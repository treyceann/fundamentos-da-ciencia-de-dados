def loop_for():
    paises = ["Brasil", "Argentina", "Africa do Sul", "Portugal"]
    idiomas = ["português", "espanhol", "inglês", "português"]
    #print(paises, type (paises))

    for pais, idioma in zip (paises, idiomas):
        #print(f'o nome do país é: {pais}')
        #print(f'o idioma do país é: {idioma}')
        teste = [pais.upper() for pais in idiomas]
        print(teste)

    novos_paises = ["Alemanha", "Egito", "Japão"]
    for pais in novos_paises:
        caixa_alta = pais.upper()
        paises.append(caixa_alta)
        print(paises)

def loop_for_2():
    paises = ["Suriname", "Guiana Francesa", "Inglaterra", "Espanha", "Áustria", "Itália", "Uruguai", "Canadá", "México", "Equador"]
    idioma = ["neerlandês", "francês", "inglês", "espanhol", "alemão", "italiano", "espanhol", "inglês/francês", "espanhol","espanhol"]
    continente = ["América", "América", "Europa", "Europa", "Europa", "Europa", "América", "América", "América", "América"]
    população = ["586.634", "294.071", "55,98 milhões", "47,35 milhões", "8,917 milhões", "59,55 milhões", "3,474 milhões", "38,01 milhões", "128,9 milhões", "17,64 milhões"]

    for pais, idioma, continente, população in zip (paises, idioma, continente, população):
        print(f'o nome do país é: {pais}')
        print(f'o idioma falado no país é: {idioma}')
        print(f'o continente do país é: {continente}')
        print(f'a população do continente é: {população}')

def range_enumerate():
    novos_paises = ["Alemanha", "Egito", "Japão"]
    for i in range(20,50,5):
        print(i)
    for index, pais in enumerate(novos_paises, start=1):
        print(index,pais)

def loop_while():
    print("loop_while")
    a = 10
    while a>0:
        print(a)
        a-=1
        #a=a-1 ou a-=1 são equivalentes; diminui o número antes do igual

def exemplo_loop_while():
    contador = 240 
    while contador < 0: 
        url: "https://www.gov.br/mre/pt-br/canais_atendimento/imprensa/notas-a-imprensa?b_start:int="
        url = url + str(contador)
        contador = contador-30
        print(url)

def main():
    #loop_for()
    #range_enumerate()
    #loop_while()
    #exemplo_loop_while()
    loop_for_2()

if __name__ == "__main__":
    main()