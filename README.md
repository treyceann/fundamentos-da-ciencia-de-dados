# Fundamentos de ciência de dados 

## Comandos para versionamento (git)

Digitar os comandos abaixo na raíz do projeto para versionar o código e subí-lo no gitlab

```
git add .
git commit -m "insira a mensagem aqui"
git pull origin main
git push origin main
```

## Criação do ambiente virtual (conda)

```
conda activate env_ treyce
conda env update
```
## Ativar ambiente virtual 

git pull origin main && conda activate env_treyce && conda env update --prune

## Comandos básicos para o terminal Linux

```
ls
cd
mkdir
history
cat

```
## Tarefa para dia 05/05 

Acrescentar 10 países, colocar idiomas, população e continente 
Ex: "Português, 200 milhões, América" 

## Tarefa para dia 12/05 

- Passar o input com o nome de um país, caso seja satisfeito, printar as características do país, pelo menos 3 países. Caso não for satisfeito, apontar com "else" que o país não consta.
- Utilizar o return (passar lista de países já existentes e acrescentar um novo país à lista)
- Trazer o tema de TCC + indicação de três fontes de dados que são importantes para as pesquisas pessoais (ex: sites de vídeos, artigos acadêmicos ou notícias)

## Tarefas para dia 19/05
 
- Passar as informações dos países, utilizando o return, o break e o continue
- Utilizar informações de idioma, países, continentes e população

## Tarefa para dia 26/05
- Replicar estrutura do acessar_pagina para todos os sites escolhidos